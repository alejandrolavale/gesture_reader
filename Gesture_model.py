import pandas as pd

df = pd.read_csv("data_testing/Alejandro_shaking_results.csv")

class GestureReader:
    """Class that gets gestures from a DB

        This class reads landmarks X and Y coordinates from a database created with full_pipeline_test.py script in triton
        repository and populates it with two new boolean columns indicating wether a gesture has been detected or not. 
    """


    def __init__(self, face_speed: float = 0.1, x_constant: float = 0.25, y_constant: float = 0.25):
        """Constructor method for the GestureReader class.

        Args:
            face_speed (float): Float value for considering the face has moved from one frame to the other. 

            x_constant (float): Float value for increasing/decreasing the gesture_threshold for the x movement.

            y_constant (float): Float value for increasing/decreasing the gesture_threshold for the y movement.
        
        """
        #TODO : Find suitable values for this landmark model.
        #Possible solution, adjust values to the demanding side and consider a gesture only if a 1 appears in a rolling window.
        self.face_speed = face_speed
        self.x_constant = x_constant
        self.y_constant = y_constant


    def clean_gestures_landmarks(self, df: pd.DataFrame, column_name: str) -> pd.DataFrame:
        """Function for getting X and Y coordinates

        This function gets a dataframe and a column name with the data as  amd creates two other columns with
        the X and Y coordinates as a float per row.

        Args:
            df (pd.Dataframe): The pandas dataframe we want to pass.
            column_name (str): The name of the column we want to clean the landmarks off.

        Returns:  
            pd.Dataframe (pd.Dataframe): Pandas dataframe with columns' string cleaned and with a list of landmark coords as float. 
        """
        df[column_name] = [string.split(' ') for string in df[column_name]]
        df[column_name] = [[string.replace('[', '') for string in coords_list] for coords_list in df[column_name]]
        df[column_name] = [[string.replace(']', '') for string in coords_list] for coords_list in df[column_name]]
        df[column_name] = [[char for  char in coords_list if char != ''] for coords_list in df[column_name]]
        df[column_name] = [[float(char) for char in coords_list] for coords_list in df[column_name]]
        df[f"{column_name}_X_coord"] = [coords_list[0] for coords_list in df[column_name]]
        df[f"{column_name}_Y_coord"] = [coords_list[1] for coords_list in df[column_name]]
        df.drop(column_name, axis=1, inplace=True)
        return df


    #This function is not needed for correct gesture modeling, just utility function.
    def get_depth_coord(self, df: pd.DataFrame , upper_landmark_col: str = 'Center_eyebrows_landmark_Y_coord', lower_landmark_col: str = 'Chin_landmark_Y_coord') -> pd.DataFrame: 
        """Function for getting the depth coordinate

        This function gets the distance in the Y axis between the upper landmark(Center_eyebrows_landmark)
        and the lower landmark(Chin_landmark) to create a similar measurement to the depth of the face. The 
        greater this distance is, the closer the face is to the camera.
        
        Args:
            df (pd.DataFrame): The pandas dataframe where the coords are.

            upper_landmark_col (str): The col with the metrics of the higher-located landmark. The landmark measurement should be a float. 

            lower_landmark_col (str): The col with the metrics of the lower-located landmark. The landmark measurement should be a float.

        Returns:
            df (pd.DataFrame): Returns the same dataframe with an added column called 'Depth_coord' that contains the measurements as a float.

        """
        df['Depth_coord'] = abs(df[lower_landmark_col] - df[upper_landmark_col]) / 200
        return df


    def get_gesture(self, df: pd.DataFrame, shaking_column_name : str = 'Shaking_gesture_bool', nodding_column_name : str = 'Nodding_gesture_bool'):
        """Get gestures reading landmark columns

        Method

        Args:
            df (pd.DataFrame): The pandas dataframe where the landmark coords are currently storaged.

            shaking_column_name (str): Name of the column for the shaking gesture.

            nodding_column_name (str): Name of the column for the nodding gesture.

        Returns:
            df (pd.Dataframe): Dataframe with gesture columns added as boolean columns.
        """
        prior_coords = [0, 0]
        x_movement_sum = 0
        y_movement_sum = 0

        person_values = df['Person'].unique()
        dfs = []

        for value in person_values:
            df_person =  df[df['Person'] == value]
            for index, _ in df_person.iterrows():    
                #Depth coordinate
                depth_shape = abs(df_person.loc[index, "Chin_landmark_Y_coord"] - df_person.loc[index, "Center_eyebrows_landmark_Y_coord"]) / 200 #This value can be changed
                
                #Width and height for thresholds
                y_shape = abs(df_person.loc[index, "Chin_landmark_Y_coord"] - df_person.loc[index, "Center_eyebrows_landmark_Y_coord"])
                x_shape = abs(df_person.loc[index, 'Right_cheek_landmark_X_coord'] - df_person.loc[index, "Left_cheek_landmark_X_coord"])

                #Minimum threshold of speed of movement
                movement_x_threshold = x_shape * depth_shape * self.face_speed  #This value can be changed
                movement_y_threshold = y_shape * depth_shape * self.face_speed  #This value can be changed

                #Minimum thres  hold that the landmark has to travel in X or Y axis for a gesture to be triggered
                x_gesture_threshold = x_shape * depth_shape * self.x_constant  #This value can be changed
                y_gesture_threshold = y_shape * depth_shape * self.y_constant  #This value can be changed

                #Calculate variation for X and Y movements from one row to another
                x_movement = abs(df_person.loc[index, 'Nose_landmark_X_coord'] - prior_coords[0])
                y_movement = abs(df_person.loc[index, 'Nose_landmark_Y_coord'] - prior_coords[1])
                
                #If movement from one frame to another is higher than movement threshold, account for that movement and keep track of it until
                #movement is not enough and return to 0 or a gesture is detected
                x_movement_sum += x_movement if x_movement >= movement_x_threshold else -x_movement_sum
                y_movement_sum += y_movement if y_movement >= movement_y_threshold else -y_movement_sum
                print(f"X Movement sum: {x_movement_sum};X Gesture_threshold: {x_gesture_threshold}")
                print(f"Y Movement sum: {y_movement_sum};Y Gesture_threshold: {y_gesture_threshold}")

                if x_movement_sum > x_gesture_threshold:
                    df_person.loc[index, shaking_column_name] = 1
                else:
                    df_person.loc[index, shaking_column_name] = 0
                
                if y_movement_sum > y_gesture_threshold:
                    df_person.loc[index, nodding_column_name] = 1
                else:
                    df_person.loc[index, nodding_column_name] = 0

                #Save coordinates detected to compare to next iteration
                prior_coords[0] = df_person.loc[index, 'Nose_landmark_X_coord']
                prior_coords[1] = df_person.loc[index, 'Nose_landmark_Y_coord']

            dfs.append(df_person)

        df = pd.concat(dfs).set_index('timestamp').sort_values('timestamp').reset_index()

        return df

#Create class instance
Gesture_detector = GestureReader()

#Clean columns 
df_clean = Gesture_detector.clean_gestures_landmarks(df, 'Nose_landmark')
df_clean = Gesture_detector.clean_gestures_landmarks(df, 'Chin_landmark')
df_clean = Gesture_detector.clean_gestures_landmarks(df, 'Center_eyebrows_landmark')
df_clean = Gesture_detector.clean_gestures_landmarks(df, 'Right_cheek_landmark')
df_clean = Gesture_detector.clean_gestures_landmarks(df, 'Left_cheek_landmark')

#Create gestures columns
df_gestured = Gesture_detector.get_gesture(df_clean)

#Test
#print(df_gestured.head(20))

#Save
df_gestured.to_csv('data_results/Current_results.csv', index=False)
df_gestured.to_excel('data_results/Current_results.xlsx', index=False)